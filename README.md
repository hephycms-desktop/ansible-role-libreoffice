libreoffice
=========

Install libreoffice

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.libreoffice

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
